<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Predmet
 *
 * @ORM\Table(name="predmet")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PredmetRepository")
 */
class Predmet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nazev", type="string", length=50)
     */
    private $nazev;

    /**
     * @var int
     *
     * @ORM\Column(name="pocet_kreditu", type="smallint", nullable=true)
     */
    private $pocetKreditu;

    /**
     * @var int
     *
     * @ORM\Column(name="pocet_hodin_prednasek", type="smallint", nullable=true)
     */
    private $pocetHodinPrednasek;

    /**
     * @var int
     *
     * @ORM\Column(name="pocet_hodin_cviceni", type="smallint", nullable=true)
     */
    private $pocetHodinCviceni;

    /**
     * @var string
     *
     * @ORM\Column(name="ukonceni", type="string", length=2)
     */
    private $ukonceni;

    /**
     * @var string
     *
     * @ORM\Column(name="anotace", type="text")
     */
    private $anotace;

    /**
     * @ORM\ManyToMany(targetEntity="Student", mappedBy="predmety")
     */
    private $studenti;

    /**
     * @ORM\ManyToMany(targetEntity="Pedagog", mappedBy="predmety")
     */
    private $pedagogove;

    /**
     * @ORM\OneToMany(targetEntity="VypsanyTermin", mappedBy="predmet")
     */
    private $vypsany_termin;

    public function __construct() {
        $this->studenti = new ArrayCollection();
        $this->pedagogove = new ArrayCollection();
        $this->vypsany_termin = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getStudenti()
    {
        return $this->studenti;
    }

    /**
     * @param mixed $studenti
     */
    public function setStudenti($studenti)
    {
        $this->studenti = $studenti;
    }

    /**
     * @return mixed
     */
    public function getPedagogove()
    {
        return $this->pedagogove;
    }

    /**
     * @param mixed $pedagogove
     */
    public function setPedagogove($pedagogove)
    {
        $this->pedagogove = $pedagogove;
    }

    /**
     * @return mixed
     */
    public function getVypsanyTermin()
    {
        return $this->vypsany_termin;
    }

    /**
     * @param mixed $vypsany_termin
     */
    public function setVypsanyTermin($vypsany_termin)
    {
        $this->vypsany_termin = $vypsany_termin;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nazev
     *
     * @param string $nazev
     *
     * @return Predmet
     */
    public function setNazev($nazev)
    {
        $this->nazev = $nazev;

        return $this;
    }

    /**
     * Get nazev
     *
     * @return string
     */
    public function getNazev()
    {
        return $this->nazev;
    }

    /**
     * Set pocetKreditu
     *
     * @param integer $pocetKreditu
     *
     * @return Predmet
     */
    public function setPocetKreditu($pocetKreditu)
    {
        $this->pocetKreditu = $pocetKreditu;

        return $this;
    }

    /**
     * Get pocetKreditu
     *
     * @return int
     */
    public function getPocetKreditu()
    {
        return $this->pocetKreditu;
    }

    /**
     * Set pocetHodinPrednasek
     *
     * @param integer $pocetHodinPrednasek
     *
     * @return Predmet
     */
    public function setPocetHodinPrednasek($pocetHodinPrednasek)
    {
        $this->pocetHodinPrednasek = $pocetHodinPrednasek;

        return $this;
    }

    /**
     * Get pocetHodinPrednasek
     *
     * @return int
     */
    public function getPocetHodinPrednasek()
    {
        return $this->pocetHodinPrednasek;
    }

    /**
     * Set pocetHodinCviceni
     *
     * @param integer $pocetHodinCviceni
     *
     * @return Predmet
     */
    public function setPocetHodinCviceni($pocetHodinCviceni)
    {
        $this->pocetHodinCviceni = $pocetHodinCviceni;

        return $this;
    }

    /**
     * Get pocetHodinCviceni
     *
     * @return int
     */
    public function getPocetHodinCviceni()
    {
        return $this->pocetHodinCviceni;
    }

    /**
     * Set ukonceni
     *
     * @param string $ukonceni
     *
     * @return Predmet
     */
    public function setUkonceni($ukonceni)
    {
        $this->ukonceni = $ukonceni;

        return $this;
    }

    /**
     * Get ukonceni
     *
     * @return string
     */
    public function getUkonceni()
    {
        return $this->ukonceni;
    }

    /**
     * Set anotace
     *
     * @param string $anotace
     *
     * @return Predmet
     */
    public function setAnotace($anotace)
    {
        $this->anotace = $anotace;

        return $this;
    }

    /**
     * Get anotace
     *
     * @return string
     */
    public function getAnotace()
    {
        return $this->anotace;
    }
}

