<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VypsanyTermin
 *
 * @ORM\Table(name="vypsany_termin")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VypsanyTerminRepository")
 */
class VypsanyTermin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="zkratka_mistnosti", type="string", length=5)
     */
    private $zkratkaMistnosti;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datum_cas", type="datetime")
     */
    private $datumCas;

    /**
     * @var int
     *
     * @ORM\Column(name="max_pocet_prihlasenych", type="smallint", nullable=true)
     */
    private $maxPocetPrihlasenych;

    /**
     * @var string
     *
     * @ORM\Column(name="poznamka", type="string", length=200)
     */
    private $poznamka;

    /**
     * @return mixed
     */
    public function getPedagog()
    {
        return $this->pedagog;
    }

    /**
     * @param mixed $pedagog
     */
    public function setPedagog($pedagog)
    {
        $this->pedagog = $pedagog;
    }

    /**
     * @return mixed
     */
    public function getPredmet()
    {
        return $this->predmet;
    }

    /**
     * @param mixed $predmet
     */
    public function setPredmet($predmet)
    {
        $this->predmet = $predmet;
    }

    /**
     * @return mixed
     */
    public function getMistnost()
    {
        return $this->mistnost;
    }

    /**
     * @param mixed $mistnost
     */
    public function setMistnost($mistnost)
    {
        $this->mistnost = $mistnost;
    }

    /**
     * @return mixed
     */
    public function getZapsaneTerminy()
    {
        return $this->zapsane_terminy;
    }

    /**
     * @param mixed $zapsane_terminy
     */
    public function setZapsaneTerminy($zapsane_terminy)
    {
        $this->zapsane_terminy = $zapsane_terminy;
    }

    /**
     * Many vypsane_terminy have One Pedagogove.
     * @ORM\ManyToOne(targetEntity="Pedagog", inversedBy="vypsany_termin")
     * @ORM\JoinColumn(name="pedagog_id", referencedColumnName="id")
     */
    private $pedagog;

    /**
     * Many vypsane_terminy have One Predmety.
     * @ORM\ManyToOne(targetEntity="Predmet", inversedBy="vypsany_termin")
     * @ORM\JoinColumn(name="predmet_id", referencedColumnName="id")
     */
    private $predmet;

    /**
     * One vypsane_terminy has One Mistnosti.
     * @ORM\OneToOne(targetEntity="Mistnost", inversedBy="vypsane_terminy")
     * @ORM\JoinColumn(name="mistnost_id", referencedColumnName="id")
     */
    private $mistnost;

    /**
     * One vypsane_terminy has Many zapsane_terminy.
     * @ORM\OneToMany(targetEntity="ZapsanyTermin", mappedBy="vypsane_terminy")
     */
    private $zapsane_terminy;

    public function __construct() {
        $this->zkratkaMistnosti = "";
        $this->poznamka = "";
        $this->datumCas = new \DateTime();
        $this->zapsane_terminy = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set zkratkaMistnosti
     *
     * @param string $zkratkaMistnosti
     *
     * @return VypsanyTermin
     */
    public function setZkratkaMistnosti($zkratkaMistnosti)
    {
        $this->zkratkaMistnosti = $zkratkaMistnosti;

        return $this;
    }

    /**
     * Get zkratkaMistnosti
     *
     * @return string
     */
    public function getZkratkaMistnosti()
    {
        return $this->zkratkaMistnosti;
    }

    /**
     * Set datumCas
     *
     * @param \DateTime $datumCas
     *
     * @return VypsanyTermin
     */
    public function setDatumCas($datumCas)
    {
        $this->datumCas = $datumCas;

        return $this;
    }

    /**
     * Get datumCas
     *
     * @return \DateTime
     */
    public function getDatumCas()
    {
        return $this->datumCas;
    }

    /**
     * Set maxPocetPrihlasenych
     *
     * @param integer $maxPocetPrihlasenych
     *
     * @return VypsanyTermin
     */
    public function setMaxPocetPrihlasenych($maxPocetPrihlasenych)
    {
        $this->maxPocetPrihlasenych = $maxPocetPrihlasenych;

        return $this;
    }

    /**
     * Get maxPocetPrihlasenych
     *
     * @return int
     */
    public function getMaxPocetPrihlasenych()
    {
        return $this->maxPocetPrihlasenych;
    }

    /**
     * Set poznamka
     *
     * @param string $poznamka
     *
     * @return VypsanyTermin
     */
    public function setPoznamka($poznamka)
    {
        $this->poznamka = $poznamka;

        return $this;
    }

    /**
     * Get poznamka
     *
     * @return string
     */
    public function getPoznamka()
    {
        return $this->poznamka;
    }

    public function jeStudentZapsany(Student $student)
    {
        /** @var ZapsanyTermin $zapsanyTermin */
        foreach($this->zapsane_terminy as $zapsanyTermin)
        {
            if($zapsanyTermin->getStudent()->getId() == $student->getId())
            {
                return $zapsanyTermin;
            }
        }

        return null;
    }
}

