<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vysledek
 *
 * @ORM\Table(name="vysledek")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VysledekRepository")
 */
class Vysledek
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="popis", type="string", length=20)
     */
    private $popis;

    /**
     * @var string
     *
     * @ORM\Column(name="typ", type="string", length=2)
     */
    private $typ;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set popis
     *
     * @param string $popis
     *
     * @return Vysledek
     */
    public function setPopis($popis)
    {
        $this->popis = $popis;

        return $this;
    }

    /**
     * Get popis
     *
     * @return string
     */
    public function getPopis()
    {
        return $this->popis;
    }

    /**
     * Set typ
     *
     * @param string $typ
     *
     * @return Vysledek
     */
    public function setTyp($typ)
    {
        $this->typ = $typ;

        return $this;
    }

    /**
     * Get typ
     *
     * @return string
     */
    public function getTyp()
    {
        return $this->typ;
    }
}

