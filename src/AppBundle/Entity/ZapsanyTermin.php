<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ZapsanyTermin
 *
 * @ORM\Table(name="zapsany_termin")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ZapsanyTerminRepository")
 */
class ZapsanyTermin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * One zapsane_terminy has One vypsane_terminy.
     * @ORM\OneToOne(targetEntity="VypsanyTermin", inversedBy="zapsane_terminy")
     * @ORM\JoinColumn(name="vypsany_termin_id", referencedColumnName="id")
     */
    private $vypsane_terminy;

    /**
     * @var Student
     * @ORM\ManyToOne(targetEntity="Student", inversedBy="zapsane_terminy")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id")
     */
    private $student;

    /**
     * @return Student
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * @param Student $student
     */
    public function setStudent($student)
    {
        $this->student = $student;
    }

    /**
     * @return mixed
     */
    public function getVypsaneTerminy()
    {
        return $this->vypsane_terminy;
    }

    /**
     * @param mixed $vypsane_terminy
     */
    public function setVypsaneTerminy($vypsane_terminy)
    {
        $this->vypsane_terminy = $vypsane_terminy;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}

