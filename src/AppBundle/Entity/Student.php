<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Student
 *
 * @ORM\Table(name="student")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StudentRepository")
 */
class Student
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="jmeno", type="string", length=30)
     */
    private $jmeno;

    /**
     * @var string
     *
     * @ORM\Column(name="prijmeni", type="string", length=30)
     */
    private $prijmeni;

    /**
     * @var string
     *
     * @ORM\Column(name="heslo", type="string", length=20)
     */
    private $heslo;

    /**
     * @ORM\ManyToMany(targetEntity="Predmet", inversedBy="studenti")
     * @ORM\JoinTable(name="studenti_predmety")
     */
    private $predmety;

    /**
     * @return mixed
     */
    public function getPredmety()
    {
        return $this->predmety;
    }

    /**
     * @param mixed $predmety
     */
    public function setPredmety($predmety)
    {
        $this->predmety = $predmety;
    }

    /**
     * @return ArrayCollection
     */
    public function getZapsaneTerminy()
    {
        return $this->zapsane_terminy;
    }

    /**
     * @param ArrayCollection $zapsane_terminy
     */
    public function setZapsaneTerminy($zapsane_terminy)
    {
        $this->zapsane_terminy = $zapsane_terminy;
    }

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="ZapsanyTermin", mappedBy="student")
     */
    private $zapsane_terminy;

    public function __construct() {
        $this->predmety = new ArrayCollection();
        $this->zapsane_terminy = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jmeno
     *
     * @param string $jmeno
     *
     * @return Student
     */
    public function setJmeno($jmeno)
    {
        $this->jmeno = $jmeno;

        return $this;
    }

    /**
     * Get jmeno
     *
     * @return string
     */
    public function getJmeno()
    {
        return $this->jmeno;
    }

    /**
     * Set prijmeni
     *
     * @param string $prijmeni
     *
     * @return Student
     */
    public function setPrijmeni($prijmeni)
    {
        $this->prijmeni = $prijmeni;

        return $this;
    }

    /**
     * Get prijmeni
     *
     * @return string
     */
    public function getPrijmeni()
    {
        return $this->prijmeni;
    }

    /**
     * Set heslo
     *
     * @param string $heslo
     *
     * @return Student
     */
    public function setHeslo($heslo)
    {
        $this->heslo = $heslo;

        return $this;
    }

    /**
     * Get heslo
     *
     * @return string
     */
    public function getHeslo()
    {
        return $this->heslo;
    }

    function __toString()
    {
        return "Student: {$this->jmeno} {$this->prijmeni} - {$this->id}";
    }
}

