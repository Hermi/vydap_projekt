<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mistnost
 *
 * @ORM\Table(name="mistnost")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MistnostRepository")
 */
class Mistnost
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="popis", type="string", length=30)
     */
    private $popis;

    /**
     * @var int
     *
     * @ORM\Column(name="kapacita", type="smallint", nullable=true)
     */
    private $kapacita;

    /**
     * One vypsane_terminy has One Mistnosti.
     * @ORM\OneToOne(targetEntity="VypsanyTermin", mappedBy="vypsane_terminy")
     */
    private $vypsany_termin;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set popis
     *
     * @param string $popis
     *
     * @return Mistnost
     */
    public function setPopis($popis)
    {
        $this->popis = $popis;

        return $this;
    }

    /**
     * Get popis
     *
     * @return string
     */
    public function getPopis()
    {
        return $this->popis;
    }

    /**
     * Set kapacita
     *
     * @param integer $kapacita
     *
     * @return Mistnost
     */
    public function setKapacita($kapacita)
    {
        $this->kapacita = $kapacita;

        return $this;
    }

    /**
     * Get kapacita
     *
     * @return int
     */
    public function getKapacita()
    {
        return $this->kapacita;
    }
}

