<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Pedagog
 *
 * @ORM\Table(name="pedagog")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PedagogRepository")
 */
class Pedagog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="jmeno", type="string", length=30)
     */
    private $jmeno;

    /**
     * @var string
     *
     * @ORM\Column(name="prijmeni", type="string", length=30)
     */
    private $prijmeni;

    /**
     * @var string
     *
     * @ORM\Column(name="tituly_pred_jmenem", type="string", length=20)
     */
    private $titulyPredJmenem;

    /**
     * @var string
     *
     * @ORM\Column(name="tituly_za_jmenem", type="string", length=20)
     */
    private $titulyZaJmenem;
    /**
     * @var string
     *
     * @ORM\Column(name="heslo", type="string", length=20)
     */
    private $heslo;
    /**
     * @ORM\ManyToMany(targetEntity="Predmet", inversedBy="pedagogove")
     * @ORM\JoinTable(name="pedagogove_predmety")
     */
    private $predmety;
    /**
     * One Pedagog has Many vypsane_terminy.
     * @ORM\OneToMany(targetEntity="VypsanyTermin", mappedBy="pedagog")
     */
    private $vypsany_termin;

    public function __construct()
    {
        $this->predmety = new ArrayCollection();
        $this->vypsany_termin = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getPredmety()
    {
        return $this->predmety;
    }

    /**
     * @param mixed $predmety
     */
    public function setPredmety($predmety)
    {
        $this->predmety = $predmety;
    }

    /**
     * @return mixed
     */
    public function getVypsanyTermin()
    {
        return $this->vypsany_termin;
    }

    /**
     * @param mixed $vypsany_termin
     */
    public function setVypsanyTermin($vypsany_termin)
    {
        $this->vypsany_termin = $vypsany_termin;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get jmeno
     *
     * @return string
     */
    public function getJmeno()
    {
        return $this->jmeno;
    }

    /**
     * Set jmeno
     *
     * @param string $jmeno
     *
     * @return Pedagog
     */
    public function setJmeno($jmeno)
    {
        $this->jmeno = $jmeno;

        return $this;
    }

    /**
     * Get prijmeni
     *
     * @return string
     */
    public function getPrijmeni()
    {
        return $this->prijmeni;
    }

    /**
     * Set prijmeni
     *
     * @param string $prijmeni
     *
     * @return Pedagog
     */
    public function setPrijmeni($prijmeni)
    {
        $this->prijmeni = $prijmeni;

        return $this;
    }

    /**
     * Get titulyPredJmenem
     *
     * @return string
     */
    public function getTitulyPredJmenem()
    {
        return $this->titulyPredJmenem;
    }

    /**
     * Set titulyPredJmenem
     *
     * @param string $titulyPredJmenem
     *
     * @return Pedagog
     */
    public function setTitulyPredJmenem($titulyPredJmenem)
    {
        $this->titulyPredJmenem = $titulyPredJmenem;

        return $this;
    }

    /**
     * Get titulyZaJmenem
     *
     * @return string
     */
    public function getTitulyZaJmenem()
    {
        return $this->titulyZaJmenem;
    }

    /**
     * Set titulyZaJmenem
     *
     * @param string $titulyZaJmenem
     *
     * @return Pedagog
     */
    public function setTitulyZaJmenem($titulyZaJmenem)
    {
        $this->titulyZaJmenem = $titulyZaJmenem;

        return $this;
    }

    /**
     * Get heslo
     *
     * @return string
     */
    public function getHeslo()
    {
        return $this->heslo;
    }

    /**
     * Set heslo
     *
     * @param string $heslo
     *
     * @return Pedagog
     */
    public function setHeslo($heslo)
    {
        $this->heslo = $heslo;

        return $this;
    }

    function __toString()
    {
        return "Pedagog: {$this->jmeno} {$this->prijmeni} - {$this->id}";
    }
}

