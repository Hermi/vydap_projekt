<?php

namespace AppBundle\Formy;
use AppBundle\Entity\VypsanyTermin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Created by PhpStorm.
 * User: ondrejbohac
 * Date: 07.02.17
 * Time: 20:43
 */
class VypsanyTerminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("datumCas")
            ->add("maxPocetPrihlasenych")
            ->add("poznamka")
            ->add("zkratkaMistnosti")
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => VypsanyTermin::class
        ));
    }
}