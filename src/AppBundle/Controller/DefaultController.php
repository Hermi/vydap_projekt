<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pedagog;
use AppBundle\Entity\Predmet;
use AppBundle\Entity\Student;
use AppBundle\Entity\VypsanyTermin;
use AppBundle\Entity\ZapsanyTermin;
use AppBundle\Formy\VypsanyTerminType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function dejPrihlaseneho()
    {
        $session = $this->get('session');

        $jePedagog = $session->get('jePedagog', false);
        $prihlaseny = $session->get('prihlasenyId', null);

        $entityManager = $this->getDoctrine()->getManager();

        if($prihlaseny != null)
        {
            if($jePedagog){
                return $entityManager->getRepository("AppBundle:Pedagog")->find($prihlaseny);
            }else{
                return $entityManager->getRepository("AppBundle:Student")->find($prihlaseny);
            }

        }

        return null;
    }

    /**
     * @Route("/prihlaseni", name="prihlaseni")
     */
    public function prihlaseniAction(Request $request)
    {
        $prihlaseny = $this->dejPrihlaseneho();

        if($prihlaseny != null) {
            return $this->redirectToRoute('hlavni_stranka');
        }

        $data = array();
        $form = $this->createFormBuilder($data);

        $form->add('prihlasovaci_udaj', TextType::class);
        $form->add('heslo', PasswordType::class);
        $form->add('submit', SubmitType::class);
        $form = $form->getForm();
        $form->handleRequest($request);

        $zprava = null;

        if($form->isValid())
        {
            $prihlasovaciUdaj = $form->get('prihlasovaci_udaj')->getData();
            $heslo = $form->get('heslo')->getData();

            $entityManager = $this->getDoctrine()->getManager();

            $prihlaseny = $entityManager->getRepository('AppBundle:Pedagog')
                ->findOneBy(array('id' => (int)$prihlasovaciUdaj, 'heslo' => $heslo));

            if($prihlaseny === null)
            {
                $prihlaseny = $entityManager->getRepository('AppBundle:Student')
                    ->findOneBy(array('id' => (int)$prihlasovaciUdaj, 'heslo' => $heslo));
            }

            if($prihlaseny)
            {
                $this->get('session')->set('jePedagog', $prihlaseny instanceof Pedagog);
                $this->get('session')->set('prihlasenyId', $prihlaseny->getId());

                return $this->redirectToRoute('hlavni_stranka');
            }

            $zprava = 'Nenalezen nikdo';
        }

        return $this->render(':default:prihlaseni.html.twig', array(
            'prihlasovaciForm' => $form->createView(),
            'zprava' => $zprava
        ));
    }

    /**
     * @Route("/odhlaseni", name="odhlaseni")
     */
    public function odhlaseniAction()
    {
        $this->get('session')->remove('jePedagog');
        $this->get('session')->remove('prihlasenyId');

        return $this->redirectToRoute('prihlaseni');
    }

    /**
     * @Route("/", name="hlavni_stranka")
     */
    public function hlavniStrankaAction()
    {
        $prihlaseny = $this->dejPrihlaseneho();

        if($prihlaseny === null) {
            return $this->redirectToRoute('prihlaseni');
        }

        return $this->render(':default:hlavniStranka.html.twig',
            array("prihlaseny"=>$prihlaseny, "jePedagog" => $prihlaseny instanceof Pedagog));
    }

    /**
     * @param Predmet $predmet
     * @Route("/pridej-termin/{id}", name="pridej_termin")
     */
    public function vytvorTerminAction(Predmet $predmet)
    {
        $prihlaseny = $this->dejPrihlaseneho();

        if($prihlaseny === null || $prihlaseny instanceof Student) {
            return $this->redirectToRoute('prihlaseni');
        }

        $vypsanyTermin = new VypsanyTermin();
        $vypsanyTermin->setPredmet($predmet);
        $vypsanyTermin->setPedagog($prihlaseny);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($vypsanyTermin);
        $entityManager->flush();

        return $this->redirectToRoute("uprav_termin", array("id"=>$vypsanyTermin->getId()));
    }

    /**
     * @Route("/uprav-termin/{id}", name="uprav_termin")

     */
    public function upravTerminAction(Request $request, VypsanyTermin $vypsanyTermin = null)
    {
        $prihlaseny = $this->dejPrihlaseneho();

        if($prihlaseny === null || $prihlaseny instanceof Student) {
            return $this->redirectToRoute('prihlaseni');
        }

        $form = $this->createForm(VypsanyTerminType::class, $vypsanyTermin);
        $form->handleRequest($request);

        if($form->isValid())
        {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($vypsanyTermin);
            $entityManager->flush();

            return $this->redirectToRoute("hlavni_stranka");
        }

        return $this->render(":default:upravitTermin.html.twig",array(
            "form" => $form->createView()
        ));
    }

    /**
     * @param VypsanyTermin $vypsanyTermin
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/smazej-termin/{id}", name="smaz_termin")
     */
    public function odstranTermin(VypsanyTermin $vypsanyTermin)
    {
        $prihlaseny = $this->dejPrihlaseneho();

        if($prihlaseny === null || $prihlaseny instanceof Student) {
            return $this->redirectToRoute('prihlaseni');
        }

        $entityManager = $this->getDoctrine()->getManager();

        try{
            $entityManager->remove($vypsanyTermin);
            $entityManager->flush();
        }catch(\Exception $e){
            throw new \Exception("Nemuzu odstranit termin na ktery jsou prihlaseni studenti");
        }

        return $this->redirectToRoute("hlavni_stranka");
    }

    /**
     * @param VypsanyTermin $vypsanyTermin
     * @Route("/prihlas-se-na-termin/{id}", name="prihlas_se_na_termin")
     */
    public function prihlasSeNaTerminAction(VypsanyTermin $vypsanyTermin)
    {
        $prihlaseny = $this->dejPrihlaseneho();

        if($prihlaseny === null || $prihlaseny instanceof Pedagog) {
            return $this->redirectToRoute('prihlaseni');
        }

        $zapsanytermin = new ZapsanyTermin();
        $zapsanytermin->setVypsaneTerminy($vypsanyTermin);
        $zapsanytermin->setStudent($prihlaseny);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($zapsanytermin);
        $entityManager->flush();

        return $this->redirectToRoute("hlavni_stranka");
    }

    /**
     * @param ZapsanyTermin $zapsanyTermin
     * @Route("/odhlas-termin/{id}", name="odhlas_termin")
     */
    public function odhlasSeZTerminuAction(ZapsanyTermin $zapsanyTermin)
    {
        $prihlaseny = $this->dejPrihlaseneho();

        if($prihlaseny === null || $prihlaseny instanceof Pedagog) {
            return $this->redirectToRoute('prihlaseni');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($zapsanyTermin);
        $entityManager->flush();

        return $this->redirectToRoute("hlavni_stranka");
    }
}
